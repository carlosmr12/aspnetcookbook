/****** Object:  StoredProcedure [dbo].[getPageData]    Script Date: 12/12/2012 15:38:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[getPageData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[getPageData]
GO
/****** Object:  Table [dbo].[books]    Script Date: 12/12/2012 15:38:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[books]') AND type in (N'U'))
DROP TABLE [dbo].[books]
GO
/****** Object:  Table [dbo].[books]    Script Date: 12/12/2012 15:38:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[books]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[books](
	[id] [int] NOT NULL,
	[publisher] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[isbn] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[title] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_books] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[books] ([id], [publisher], [isbn], [title]) VALUES (0, N'O''Relly', N'0-56398-653-1', N'HTML & XHTML: The Definitive Guide')
INSERT [dbo].[books] ([id], [publisher], [isbn], [title]) VALUES (1, N'O''Relly', N'0-965-654-54', N'Java CookBook')
INSERT [dbo].[books] ([id], [publisher], [isbn], [title]) VALUES (2, N'O''Relly', N'1-5637-458-1', N'JavaScript CookBook')
INSERT [dbo].[books] ([id], [publisher], [isbn], [title]) VALUES (3, N'O''Relly', N'0-5638-652-8', N'Perl CookBook')
INSERT [dbo].[books] ([id], [publisher], [isbn], [title]) VALUES (4, N'O''Relly', N'0-695-8637-2', N'Programming ASP.NET')
INSERT [dbo].[books] ([id], [publisher], [isbn], [title]) VALUES (5, N'O''Relly', N'5-6535-561-1', N'Programming C#')
INSERT [dbo].[books] ([id], [publisher], [isbn], [title]) VALUES (6, N'O''Relly', N'0-5638-5698-1', N'SQL in a Nutshell')
INSERT [dbo].[books] ([id], [publisher], [isbn], [title]) VALUES (7, N'O''Relly', N'0-96538-223-1', N'Programming in Visual Basic .NET')
INSERT [dbo].[books] ([id], [publisher], [isbn], [title]) VALUES (8, N'O''Relly', N'0-5635-151236-4', N'ADO: ActiveX Data Objects')
INSERT [dbo].[books] ([id], [publisher], [isbn], [title]) VALUES (9, N'O''Relly', N'0-6598-653-1', N'Access CookBook')
INSERT [dbo].[books] ([id], [publisher], [isbn], [title]) VALUES (10, N'O''Relly', N'0-6598-635-1', N'COM+ Programming with Visual Basic')
/****** Object:  StoredProcedure [dbo].[getPageData]    Script Date: 12/12/2012 15:38:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[getPageData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[getPageData]
@pageNumber INT,
@pageSize INT,
@totalRecords INT OUTPUT
AS
DECLARE @firstRecordInPage INT
DECLARE @lastRecordInPage INT

-- Calculate the number of rows need to get the current page
SELECT @firstRecordInPage = @pageNumber * @pageSize + 1
SELECT @lastRecordInPage = @firstRecordInPage + @pageSize

-- Create a temporary table to copy the book data into
-- Include only the columns needed with an additional ID
-- column that is the primary key of the temporary table
-- In addition, it is and identity that will number the
-- records copied into the table starting with 1 thus allowing
-- us to query only for the specific records needed for the
-- requested page.

CREATE TABLE #Book
(
	[ID] [int] IDENTITY (1, 1) NOT NULL,
	[BookID] [int] NOT NULL,
	[title] [nvarchar] (100) NOT NULL,
	[isbn] [nvarchar] (50) NOT NULL,
	[publisher] [nvarchar] (50) NOT NULL
)

-- Copy the data from the book table into the temp table
INSERT #Book
(BookID, title, isbn, publisher)
SELECT id, title, isbn, publisher FROM cookBook.dbo.books ORDER BY title

-- Get the rows required for the passed page
SELECT * FROM #Book
WHERE ID >= @firstRecordInPage
AND ID < @lastRecordInPage

-- Get the total number of records in the table
SELECT @totalRecords = COUNT(*) FROM cookBook.dbo.books
GO
' 
END
GO
