﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AspNetCookBook
{
    public partial class checkBoxList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Collections.ArrayList values;

            if (!IsPostBack) 
            {
                // Creating the arrayList with the books
                values = new System.Collections.ArrayList();
                values.Add("Access CookBook");
                values.Add("ASP.NET CookBook");
                values.Add("Perl CookBook");
                values.Add("Java CookBook");
                values.Add("VB.NET Language in Nutshell");
                values.Add("Programming Visual Basic.NET");
                values.Add("Programming C#");
                values.Add(".NET Framework Essentials");
                values.Add("COM and .NET Component Services");

                // Binding the arrayList with the CheckBokList
                cbBooks.DataSource = values;
                cbBooks.DataBind();

                //Preselect any books
                cbBooks.Items.FindByText("ASP.NET CookBook").Selected = true;
                cbBooks.Items.FindByText("Perl CookBook").Selected = true;
            }
        }
    }
}