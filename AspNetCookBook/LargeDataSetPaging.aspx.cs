﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AspNetCookBook
{
    public partial class LargeDataSetPaging : System.Web.UI.Page
    {
        private int currentPage;
        private int totalPages;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                // this is a post back so initialize the current and the total page variables
                // with the values currently being displayed
                currentPage = Convert.ToInt32(labCurrentPage.Text) - 1;
                totalPages = Convert.ToInt32(labTotalPages.Text);
            }
            else
            {
                // this is the first rendering of the form so set the current page to
                // the first page and bind the data
                currentPage = 0;
                bindData();
            }
        }

        private void bindData()
        {
            SqlConnection dbConn = null;
            String strConnection;
            String strSQL;
            SqlCommand dCmd;
            SqlParameter param;
            SqlDataReader dReader = null;
            int totalRecords = 0;

            try
            {
                // get the connection string from web.config and open a connection to the database
                strConnection = ConfigurationManager.ConnectionStrings["cookBookConnectionString"].ConnectionString;
                dbConn = new SqlConnection(strConnection);
                dbConn.Open();

                // create command to execute the stored procedure along with the
                // parameters required in/out of the procedure
                strSQL = "cookBook.dbo.getPageData";
                dCmd = new SqlCommand(strSQL, dbConn);
                dCmd.CommandType = CommandType.StoredProcedure;

                param = dCmd.Parameters.Add("pageNumber", SqlDbType.Int);
                param.Direction = ParameterDirection.Input;
                param.Value = currentPage;

                param = dCmd.Parameters.Add("pageSize", SqlDbType.Int);
                param.Direction = ParameterDirection.Input;
                param.Value = dgBooks.PageSize;

                param = dCmd.Parameters.Add("totalRecords", SqlDbType.Int);
                param.Direction = ParameterDirection.Output;

                // execute the stored procedure and set the datasource for the datagrid
                dReader = dCmd.ExecuteReader();
                dgBooks.DataSource = dReader;
                dgBooks.DataBind();

                // close the datareader to make the output parameter available
                dReader.Close();

                // output information about the current page and total number of pages
                totalRecords = Convert.ToInt32(dCmd.Parameters["totalRecords"].Value);
                totalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(totalRecords) / dgBooks.PageSize));
                
                labTotalPages.Text = totalPages.ToString();
                labCurrentPage.Text = (currentPage + 1).ToString();
            }

            finally
            {
                // cleanup
                if (dbConn != null)
                {
                    dbConn.Close();
                }
            }
        }

        protected void btnFirst_ServerClick(Object sender,
                                            System.EventArgs e)
        {
            if (currentPage > 0)
            {
                currentPage = 0;
                bindData();
            }
        }

        protected void btnPrev_ServerClick(Object sender,
                                            System.EventArgs e)
        {
            if (currentPage > 0)
            {
                currentPage -= 1;
                bindData();
            }
        }

        protected void btnNext_ServerClick(Object sender,
                                            System.EventArgs e)
        {
            if (currentPage < totalPages - 1)
            {
                currentPage += 1;
                bindData();
            }
        }

        protected void btnLast_ServerClick(Object sender,
                                            System.EventArgs e)
        {
            if (currentPage < totalPages - 1)
            {
                currentPage = totalPages - 1;
                bindData();
            }
        }
    }
}