﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="LargeDataSetPaging.aspx.cs" Inherits="AspNetCookBook.LargeDataSetPaging" %>

<asp:Content ID="pageBody" runat="server" ContentPlaceHolderID="PageBody">
    <div class="pageHeading" align="center">
        <asp:DataGrid ID="dgBooks" runat="server" AllowPaging="True" AllowCustomPaging="True"
            AutoGenerateColumns="False" BorderColor="#000080" BorderWidth="2px" 
            HorizontalAlign="Center" PageSize="10" Width="90%"
            PagerStyle-HorizontalAlign="Center"
            PagerStyle-Visible="False">
            <HeaderStyle CssClass="tableHeader" HorizontalAlign="Center" />
            <Columns>
                <asp:BoundColumn DataField="title" HeaderText="Title" />
                <asp:BoundColumn DataField="isbn" HeaderText="ISBN">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="publisher" HeaderText="Publisher">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundColumn>
            </Columns>            
        </asp:DataGrid>
        <br />
        <table align="center">
            <tr>
                <td colspan="4" align="center">
                    Displaying page
                    <asp:Literal ID="labCurrentPage" runat="server"></asp:Literal> of
                    <asp:Literal ID="labTotalPages" runat="server"></asp:Literal>
                </td>
                <td align="center">
                    <input id="btnFirst" runat="server" type="button" value="First" onserverclick="btnFirst_ServerClick" />
                </td>
                <td align="center">
                    <input id="btnPrev" runat="server" type="button" value="Prev" onserverclick="btnPrev_ServerClick" />
                </td>
                <td align="center">
                    <input id="btnNext" runat="server" type="button" value="Next" onserverclick="btnNext_ServerClick" />
                </td>
                <td align="center">
                    <input id="btnLast" runat="server" type="button" value="Last" onserverclick="btnLast_ServerClick" />
                </td>
            </tr>
        </table>
        DataGrid using NEXT and PREV as Handle Navigation
    </div>

</asp:Content>
