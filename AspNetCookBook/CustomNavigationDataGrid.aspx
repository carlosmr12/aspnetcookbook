﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="CustomNavigationDataGrid.aspx.cs" Inherits="AspNetCookBook.CustomNavigationDataGrid" %>

<asp:Content ID="pageBody" runat="server" ContentPlaceHolderID="PageBody">
    <div class="pageHeading" align="center">
        <asp:DataGrid ID="dgBooks" runat="server" AllowPaging="True" 
            AutoGenerateColumns="False" BorderColor="#000080" BorderWidth="2px" 
            HorizontalAlign="Center" PageSize="5" Width="90%"
            PagerStyle-HorizontalAlign="Center"
            PagerStyle-Visible="False"
            AllowSorting="True"
            OnSortCommand="dgBooks_SortCommand">
            <HeaderStyle CssClass="tableHeader" HorizontalAlign="Center" />
            <Columns>
                <asp:BoundColumn DataField="title" HeaderText="Title" SortExpression="title" />
                <asp:BoundColumn DataField="isbn" HeaderText="ISBN" SortExpression="isbn">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="publisher" HeaderText="Publisher" SortExpression="publisher">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundColumn>
            </Columns>            
        </asp:DataGrid>
        <br />
        <table border="0" align="center" style="width: 36%">
            <tr>
                <td style="width: 408px">                    
                    <asp:Label ID="lblPager" runat="server" CssClass="pagerText" Text="Label"></asp:Label>                    
                </td>
                <td>                
                    <asp:TextBox ID="txtNewPageNumber" runat="server" style="margin-left: 0px" 
                        Width="56px"></asp:TextBox>                
                </td>
                <td>                
                    <asp:Button ID="btnDisplayPage" runat="server" onclick="btnDisplayPage_Click" 
                        Text="Update" />
                </td>
            </tr>
        </table>
        DataGrid using NEXT and PREV as Handle Navigation
    </div>

</asp:Content>
