﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AspNetCookBook
{
    public partial class NextPrevDataGrid : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindData();
            }
        }

        protected void dgBooks_PageIndexChanged(Object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        { 
            // set new page index and rebind the data
            dgBooks.CurrentPageIndex = e.NewPageIndex;
            bindData();
        }

        private void bindData()
        {
            SqlConnection dbConn = null;
            SqlDataAdapter da = null;
            DataSet dSet = null;
            String strConnection;
            String strSQL;

            try
            {
                // get the connection string from web.config and open a connection to the database
                strConnection = ConfigurationManager.ConnectionStrings["cookBookConnectionString"].ConnectionString;
                dbConn = new SqlConnection(strConnection);
                dbConn.Open();

                // build the query string and get the data from the data base

                strSQL = "SELECT title, isbn, publisher FROM cookBook.dbo.Book ORDER BY title";
                da = new SqlDataAdapter(strSQL, dbConn);

                dSet = new DataSet();
                da.Fill(dSet);

                // set the source of the data for the Datagrid (DataView) control and bind it
                dgBooks.DataSource = dSet;
                dgBooks.DataBind();
            }

            finally
            { 
                // cleanup
                if (dbConn != null)
                {
                    dbConn.Close();
                }
            }
        }
    }
}