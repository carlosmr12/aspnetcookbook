﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AspNetCookBook
{
    public partial class DataGridWithEditing : System.Web.UI.Page
    {
        // the following variable contains the list of yes/no selection used in
        // the dropbox lists and is declared protected to provide access to the data
        // from the aspx page
        protected System.Collections.ArrayList yesNoSelections;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                bindData();
            }

        }

        private void bindData() 
        {
            SqlConnection dbConn = null;
            SqlDataAdapter da = null;
            String strConnection;
            String strSQL;
            DataTable dTable = null;


            try
            {
                // get the connection string from web.config and open a connection to the database
                strConnection = ConfigurationManager.ConnectionStrings["cookBookConnectionString"].ConnectionString;
                dbConn = new SqlConnection(strConnection);
                dbConn.Open();

                // build the query string and get the data from the database
                strSQL = "SELECT EditProblemID, SectionNumber, SectionHeading, HasExample FROM cookBook.dbo.EditProblem ORDER BY SectionNumber";
                da = new SqlDataAdapter(strSQL, dbConn);
                dTable = new DataTable();
                da.Fill(dTable);

                // build the array yes/no with the acceptable responses to the "Has C# Sample" field
                yesNoSelections = new System.Collections.ArrayList(2);
                yesNoSelections.Add(new ListItem("No","0"));
                yesNoSelections.Add(new ListItem("Yes", "1"));

                // set the source of the data for the datagrid control and bind it
                dgProblems.DataSource = dTable;
                dgProblems.DataKeyField = "EditProblemID";
                dgProblems.DataBind();
            
            }

            finally
            {
                // cleanup
                if (dbConn != null)
                {
                    dbConn.Close();
                }
            }
        }

        protected void dgProblems_CancelCommand(object source, DataGridCommandEventArgs e)
        {

            dgProblems.EditItemIndex = -1;
            bindData();

        }

        protected void dgProblems_EditCommand(object source, DataGridCommandEventArgs e)
        {

            dgProblems.EditItemIndex = e.Item.ItemIndex;
            bindData();

        }

        protected void dgProblems_UpdateCommand(object source, DataGridCommandEventArgs e)
        {

            SqlConnection dbConn = null;
            SqlCommand dCmd = null;
            String sectionHeading = null;
            int hasCSSample;
            String strConnection;
            String strSQL;
            int rowsAffected;
            DropDownList ddl = null;

            try
            {
                sectionHeading = ((TextBox)(e.Item.FindControl("txtSectionHeading"))).Text;
                ddl = (DropDownList)(e.Item.FindControl("selHasCSSample"));
                hasCSSample = Convert.ToInt32(ddl.SelectedItem.Value);

                // get the connection string from the web.config file
                strConnection = ConfigurationManager.ConnectionStrings["cookBookConnectionString"].ConnectionString;
                dbConn = new SqlConnection(strConnection);
                dbConn.Open();

                strSQL = "UPDATE cookBook.dbo.EditProblem SET SectionHeading='" + sectionHeading + "', HasExample=" + hasCSSample + "WHERE EditProblemID=" + dgProblems.DataKeys[e.Item.ItemIndex].ToString();
                dCmd = new SqlCommand(strSQL, dbConn);
                rowsAffected = dCmd.ExecuteNonQuery();

                // reset the edit item and rebind the data
                dgProblems.EditItemIndex = -1;
                bindData();
            }

            finally
            { 
                // cleanup
                if (dbConn != null)
                {
                    dbConn.Close();
                }
            }
        }
    }
}