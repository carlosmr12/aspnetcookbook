﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AspNetCookBook
{
    public partial class DataListHashTable : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Collections.Hashtable values;

            if (!IsPostBack)
            {
                values = new System.Collections.Hashtable();
                values.Add(".NET FrameWork Essentials",
                            "images/books/DotNetFrameworkEssentials.gif");
                values.Add("Access CookBook",
                            "images/books/AccessCookBook.gif");
                values.Add("ASP.NET CookBook",
                            "images/books/ASPNetCookBook.gif");
                values.Add("Java CookBook",
                            "images/books/JavaCookBook.gif");
                values.Add("JavaScript Application CookBook",
                            "images/books/JavaScriptCookBook.gif");
                values.Add("Programming C#",
                            "images/books/ProgrammingCSharp.gif");
                values.Add("Programming Visual Basic .NET",
                            "images/books/ProgrammingVBDotNet.gif");
                values.Add("VB .NET Language in a Nutshell",
                            "images/books/VBDotNetInANutshell.gif");
                
                dlBooks.DataSource = values;
                dlBooks.DataKeyField = "Key";
                dlBooks.DataMember = "Value";
                dlBooks.DataBind();
            }

        }
    }
}