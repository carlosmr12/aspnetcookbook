﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="NextPrevDataGrid.aspx.cs" Inherits="AspNetCookBook.NextPrevDataGrid" %>

<asp:Content ID="pageBody" runat="server" ContentPlaceHolderID="PageBody">
    <div class="pageHeading" align="center">
        <asp:DataGrid ID="dgBooks" runat="server" AllowPaging="True" 
            AutoGenerateColumns="False" BorderColor="#000050" BorderWidth="2px" 
            HorizontalAlign="Center" PageSize="5" Width="90%"
            PagerStyle-HorizontalAlign="Center"
            PagerStyle-Mode="NextPrev"
            PagerStyle-Position="Bottom"
            PagerStyle-NextPageText="Next"
            PagerStyle-PrevPageText="Prev"
            OnPageIndexChanged="dgBooks_PageIndexChanged">
            <HeaderStyle CssClass="tableHeader" HorizontalAlign="Center" />
            <Columns>
                <asp:BoundColumn DataField="title" HeaderText="Title" />
                <asp:BoundColumn DataField="isbn" HeaderText="ISBN">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="publisher" HeaderText="Publisher">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundColumn>
            </Columns>            
        </asp:DataGrid>
        DataGrid using NEXT and PREV as Handle Navigation
    </div>

</asp:Content>