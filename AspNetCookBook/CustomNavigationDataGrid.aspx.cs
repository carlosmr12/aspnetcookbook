﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AspNetCookBook
{
    public partial class CustomNavigationDataGrid : System.Web.UI.Page
    {

        // the following enumeration is used to define the sort orders
        private enum enuSortOrder
        {
            soAscending = 0,
            soDescending = 1
        }

        // strings to use for the sort expressions and column title
        // separate arrays are used to support the sort expression and titles
        // being different
        static readonly String[] sortExpression = new String[] { "title", "isbn", "publisher"};
        static readonly String[] columnTitle = new String[] { "title", "isbn", "publisher" };

        // the names of the variables placed in the viewstate
        static readonly String VS_CURRENT_SORT_EXPRESSION = "currentSortExpression";
        static readonly String VS_CURRENT_SORT_ORDER = "currentSortOrder";


        protected void Page_Load(object sender, EventArgs e)
        {
            String defaultSortExpression;
            enuSortOrder defaultSortOrder;

            if (!IsPostBack)
            { 
                // sort by title, ascending as default
                defaultSortExpression = sortExpression[0];
                defaultSortOrder = enuSortOrder.soAscending;

                // binding the data to the grid
                this.ViewState.Add(VS_CURRENT_SORT_EXPRESSION, defaultSortExpression);
                this.ViewState.Add(VS_CURRENT_SORT_ORDER, defaultSortOrder);
                bindData(defaultSortExpression, defaultSortOrder);
            }
        }

        protected void dgBooks_SortCommand(Object Source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            String newSortExpression = null;
            String currentSortExpression = null;
            enuSortOrder currentSortOrder;

            //get the current sort expression and order from the viewstate
            currentSortExpression = (String)(this.ViewState[VS_CURRENT_SORT_EXPRESSION]);
            currentSortOrder = (enuSortOrder)(this.ViewState[VS_CURRENT_SORT_ORDER]);

            //check to see if this is a new column or the sort order
            //of the current column needs to be changed
            newSortExpression = e.SortExpression;
            if (newSortExpression.Equals(currentSortExpression))
            {
                //sort column is the same so change the sort order
                if (currentSortOrder == enuSortOrder.soAscending)
                {
                    currentSortOrder = enuSortOrder.soDescending;
                }
                else
                {
                    currentSortOrder = enuSortOrder.soAscending;
                }
            }
            else
            { 
                // sort column is different so set the new column with ascending
                // sort order
                currentSortExpression = newSortExpression;
                currentSortOrder = enuSortOrder.soAscending;
            }

            // update the viewstate with the new sort information
            this.ViewState.Add(VS_CURRENT_SORT_EXPRESSION, currentSortExpression);
            this.ViewState.Add(VS_CURRENT_SORT_ORDER, currentSortOrder);
            bindData(currentSortExpression, currentSortOrder);
        }

        protected void btnDisplayPage_Click(object sender, EventArgs e)
        {
            String currentSortExpression = (String)(this.ViewState[VS_CURRENT_SORT_EXPRESSION]);
            enuSortOrder currentSortOrder = (enuSortOrder)(this.ViewState[VS_CURRENT_SORT_ORDER]);

            dgBooks.CurrentPageIndex = Convert.ToInt32(txtNewPageNumber.Text) - 1;
            bindData(currentSortExpression, currentSortOrder);
        }

        private void bindData(String sortExpression, enuSortOrder sortOrder)
        {
            SqlConnection dbConn = null;
            SqlDataAdapter da = null;
            DataSet dSet = null;
            String strConnection;
            String strSQL;

            int index = 0;
            String colImage = null;
            DataGridColumn col = null;
            String strSortOrder = null;

            try
            {
                // get the connection string from web.config and open a connection to the database
                strConnection = ConfigurationManager.ConnectionStrings["cookBookConnectionString"].ConnectionString;
                dbConn = new SqlConnection(strConnection);
                dbConn.Open();

                // build the query string and get the data from the data base
                if (sortOrder == enuSortOrder.soAscending)
                {
                    strSortOrder = " ASC";
                }
                else
                {
                    strSortOrder = " DESC";
                }

                strSQL = "SELECT title, isbn, publisher FROM cookBook.dbo.Book ORDER BY " + sortExpression + strSortOrder;
                da = new SqlDataAdapter(strSQL, dbConn);

                dSet = new DataSet();
                da.Fill(dSet);

                // loop through the columns in the datagrid updating the head to
                // mark which column is the sort column and the sort order
                for (index = 0; index < dgBooks.Columns.Count; index++)
                {
                    col = dgBooks.Columns[index];
                    //check to see if this is the sort sort column
                    if (col.SortExpression == sortExpression)
                    {
                        // this is the sort column so determine whether the ascending or
                        // descending image needs to be included
                        if (sortOrder == enuSortOrder.soAscending)
                        {
                            colImage = "<img src='images/sort_ascending.gif' border='0'>";
                        }
                        else
                        {
                            colImage = "<img src='images/sort_descending.gif' border='0'>";
                        }
                    }
                    else
                    {
                        // this is not the sort column so include no image html
                        colImage = "";
                    }
                    // set the title for the column
                    col.HeaderText = columnTitle[index] + colImage;
                }

                // set the source of the data for the Datagrid (DataView) control and bind it
                dgBooks.DataSource = dSet;
                dgBooks.DataBind();

                lblPager.Text = "Displaying Page " + Convert.ToString(dgBooks.CurrentPageIndex + 1) + " of " + Convert.ToString(dgBooks.PageCount) + ", Enter Desired Page Number:";
            }

            finally
            {
                // cleanup
                if (dbConn != null)
                {
                    dbConn.Close();
                }
            }
        }
        
    }
}