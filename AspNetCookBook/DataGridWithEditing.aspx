﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="DataGridWithEditing.aspx.cs" Inherits="AspNetCookBook.DataGridWithEditing" %>

<asp:Content ID="pageBody" runat="server" ContentPlaceHolderID="PageBody">
    <div class="pageHeading" align="center">
        <asp:DataGrid ID="dgProblems" runat="server" AllowPaging="True" AllowCustomPaging="True"
            AutoGenerateColumns="False" BorderColor="#000080" BorderWidth="2px" 
            HorizontalAlign="Center" PageSize="10" Width="90%"
            PagerStyle-HorizontalAlign="Center"
            PagerStyle-Visible="False" oncancelcommand="dgProblems_CancelCommand" 
            oneditcommand="dgProblems_EditCommand" 
            onupdatecommand="dgProblems_UpdateCommand">
            <HeaderStyle CssClass="tableHeader" HorizontalAlign="Center" />
            <ItemStyle CssClass="tableCellNormal" />
            <Columns>
                <asp:BoundColumn DataField="SectionNumber" HeaderText="Section" ReadOnly="True" />
                <asp:TemplateColumn HeaderText="Section Heading">
                    <ItemTemplate>
                        <%#Eval("SectionHeading")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtSectionHeading" runat="server" Columns="55" CssClass="tableCellNormal" Text='<%#Eval("SectionHeading")%>' />
                    </EditItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="CS Example" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <%#yesNoSelections[Convert.ToInt32(Eval("HasExample"))]%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="selHasCSSample" runat="server" DataSource="<%# yesNoSelections %>" DataTextField="Text" DataValueField="Value" SelectedIndex='<%#Convert.ToInt32(Eval("HasExample"))%>' />
                    </EditItemTemplate>
                </asp:TemplateColumn>

                <asp:EditCommandColumn ButtonType="LinkButton" EditText="Edit" CancelText="Cancel" UpdateText="Update" />

            </Columns>            
            
        </asp:DataGrid>
        DataGrid using NEXT and PREV as Handle Navigation
    </div>

</asp:Content>

