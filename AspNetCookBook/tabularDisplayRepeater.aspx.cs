﻿using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AspNetCookBook
{
    public partial class tabularDisplayRepeater : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlDataSource dSource = null;

            if (!Page.IsPostBack)
            {
                dSource = new SqlDataSource();
                dSource.ConnectionString = ConfigurationManager.ConnectionStrings["cookBookConnectionString"].ConnectionString;
                dSource.DataSourceMode = SqlDataSourceMode.DataReader;
                dSource.ProviderName = "System.Data.SqlClient";
                dSource.SelectCommand = "SELECT title, isbn, publisher FROM cookBook.dbo.Book ORDER BY title";

                // Set the Source of the data for the gridview control and bind it
                repBooks.DataSource = dSource;
                repBooks.DataBind();
            }
        }
    }
}