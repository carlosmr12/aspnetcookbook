﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="DataListHashTable.aspx.cs" Inherits="AspNetCookBook.DataListHashTable" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageBody">
    <div align="center" class="pageHeading">
        <asp:DataList ID="dlBooks" runat="server" 
            Caption="Several Books Avaiable From O'Reilly &amp; Associates, Inc." 
            RepeatColumns="4" RepeatDirection="Horizontal">
            <HeaderStyle CssClass="BlackPageHeading" HorizontalAlign="Center" />
            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
            <ItemTemplate>
                <table align="center">
                    <tr>
                        <td align="center">
                            <img src= "<%#Eval("Value")%>" heigth="145" alt="BookImage" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <%#Eval("Key")%>
                        </td>
                    </tr>
                </table>                
            </ItemTemplate>
        </asp:DataList>
    </div>
    
</asp:Content>