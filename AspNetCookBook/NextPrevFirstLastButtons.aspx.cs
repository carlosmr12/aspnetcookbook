﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AspNetCookBook
{
    public partial class NextPrevFirstLastButtons : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindData();
            }
        }

        private void bindData()
        {
            SqlConnection dbConn = null;
            SqlDataAdapter da = null;
            DataSet dSet = null;
            String strConnection;
            String strSQL;

            try
            {
                // get the connection string from web.config and open a connection to the database
                strConnection = ConfigurationManager.ConnectionStrings["cookBookConnectionString"].ConnectionString;
                dbConn = new SqlConnection(strConnection);
                dbConn.Open();

                // build the query string and get the data from the data base

                strSQL = "SELECT title, isbn, publisher FROM cookBook.dbo.Book ORDER BY title";
                da = new SqlDataAdapter(strSQL, dbConn);

                dSet = new DataSet();
                da.Fill(dSet);

                // set the source of the data for the Datagrid (DataView) control and bind it
                dgBooks.DataSource = dSet;
                dgBooks.DataBind();
            }

            finally
            {
                // cleanup
                if (dbConn != null)
                {
                    dbConn.Close();
                }
            }
        }

        protected void btnFirst_ServerClick(Object sender,
                                            System.EventArgs e)
        {
            if (dgBooks.CurrentPageIndex > 0) 
            {
                dgBooks.CurrentPageIndex = 0;
                bindData();
            }
        }

        protected void btnPrev_ServerClick(Object sender,
                                            System.EventArgs e)
        {
            if (dgBooks.CurrentPageIndex > 0)
            {
                dgBooks.CurrentPageIndex -= 1;
                bindData();
            }
        }

        protected void btnNext_ServerClick(Object sender,
                                            System.EventArgs e)
        {
            if (dgBooks.CurrentPageIndex < dgBooks.PageCount - 1)
            {
                dgBooks.CurrentPageIndex += 1;
                bindData();
            }
        }

        protected void btnLast_ServerClick(Object sender,
                                            System.EventArgs e)
        {
            if (dgBooks.CurrentPageIndex < dgBooks.PageCount - 1)
            {
                dgBooks.CurrentPageIndex = dgBooks.PageCount - 1;
                bindData();
            }
        }
    }
}