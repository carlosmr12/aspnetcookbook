﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CH01NestedMasterPageVB.Master" CodeBehind="Default.aspx.cs" Inherits="AspNetCookBook.Default"  %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentBody">
    <div align="center" class="pageHeading">
        Nested Master Pages (VB)
    </div>
    <br />
    <asp:GridView ID="gvQuick" runat="server" AutoGenerateColumns="true"
        BorderColor="#000080" BorderWidth="2px" HorizontalAlign="Center" Width="90%">
    </asp:GridView>
    <asp:SqlDataSource ID="dSource" runat="server"></asp:SqlDataSource>
    <br />
    <p align="center">The content for your pages is placed here.</p>
</asp:Content>