﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CH01NestedMasterPageVB.Master" CodeBehind="tabularDisplayRepeater.aspx.cs" Inherits="AspNetCookBook.tabularDisplayRepeater" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentBody">
    <div align="center" class="pageHeading">
        Templates with REPEATER (VB)
        <br />
    </div>
    <table width="90%" align="center" class="tableWithBorder" style="border-collapse:collapse">
        <asp:Repeater ID="repBooks" runat="server">
            <Headertemplate>
                <thead class="tableHeader">
                    <tr>
                        <th align="center">
                            Title
                        </th>
                        <th align="center">
                            ISBN
                        </th>
                        <th align="center">
                            Publisher
                        </th>
                    </tr>
                </thead>            
            </Headertemplate>
            <ItemTemplate>
                <tr class="tableCellNormal">
                    <td>
                        <%#Eval("title") %>
                    </td>
                    <td align="center">
                        <%#Eval("isbn") %>
                    </td>
                    <td align="center">
                        <%#Eval("publisher") %>
                    </td>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="tableCellAlternating">
                    <td>
                        <%#Eval("title") %>
                    </td>
                    <td align="center">
                        <%#Eval("isbn") %>
                    </td>
                    <td align="center">
                        <%#Eval("publisher") %>
                    </td>
                </tr>  
            </AlternatingItemTemplate>
        </asp:Repeater>
    </table>
    
    <p align="center">The content for your pages is placed here.</p>
</asp:Content>
