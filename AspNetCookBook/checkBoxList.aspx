﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="checkBoxList.aspx.cs" Inherits="AspNetCookBook.checkBoxList" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageBody">
    <div align="center" class="pageHeading">
        CheckBoxList With Array
    </div>
    <div style="margin-left:50px">
        
        <asp:CheckBoxList ID="cbBooks" runat="server" CssClass="MenuItem" 
            RepeatColumns="2" RepeatDirection="Horizontal" Width="100%">
        </asp:CheckBoxList>
        
    </div>
</asp:Content>